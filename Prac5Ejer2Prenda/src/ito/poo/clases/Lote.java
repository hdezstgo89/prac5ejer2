package ito.poo.clases;

import java.time.LocalDate;

public class Lote {

	private int numLote;
	private int numPiezas;
	private LocalDate fecha;
	/**********************************************************/
	public Lote() {
		super();
	}
	public Lote(int numLote, int numPiezas, LocalDate fecha) {
		super();
		this.numLote = numLote;
		this.numPiezas = numPiezas;
		this.fecha = fecha;
	}
	/**********************************************************/
	public int getNumLote() {
		return numLote;
	}

	public void setNumLote(int numLote) {
		this.numLote = numLote;
	}

	public int getNumPiezas() {
		return numPiezas;
	}

	public void setNumPiezas(int numPiezas) {
		this.numPiezas = numPiezas;
	}

	public LocalDate getFecha() {
		return fecha;
	}

	public void setFecha(LocalDate fecha) {
		this.fecha = fecha;
	}
	/**********************************************************/
	public float costoProduccion(Prenda P) {
		float p=0;
			p = P.getCostoProduccion() * this.numPiezas;
		return p;
	}

	public float montoProduccionxlote(Prenda P) {
		float p=0;
			p = costoProduccion(P) * 0.15F;
		return p;
	}

	public float montoRecuperacionxpieza(Prenda P) {
		float p=0;
			p = P.getCostoProduccion() * 0.5F;
		return p;
	}
	/**********************************************************/
	@Override
	public String toString() {
		return "(Numero de lote: " + numLote + ", Numero de piezas: " + numPiezas + ", Fecha: " + fecha + ")";
	}
}