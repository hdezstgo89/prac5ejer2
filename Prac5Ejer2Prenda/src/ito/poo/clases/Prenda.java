package ito.poo.clases;

import java.time.LocalDate;
import java.util.ArrayList;

public class Prenda {
	
	private int modelo;
	private String tela;
	private float costoProduccion;
	private String genero;
	private String temporada;
	private ArrayList<Lote> lotes = new ArrayList<Lote>();
	/**********************************************************/
	public Prenda() {
		super();
	}
	
	public Prenda(int modelo, String genero, String temporada, String tela, float costoProduccion) {
		super();
		this.modelo = modelo;
		this.genero = genero;
		this.temporada = temporada;
		this.tela = tela;
		this.costoProduccion = costoProduccion;
	}
	/**********************************************************/
	public int getModelo() {
		return modelo;
	}
	
	public void setModelo(int modelo) {
		this.modelo = modelo;
	}

	public String getTela() {
		return tela;
	}

	public void setTela(String tela) {
		this.tela = tela;
	}

	public float getCostoProduccion() {
		return costoProduccion;
	}

	public void setCostoProduccion(float costoProduccion) {
		this.costoProduccion = costoProduccion;
	}

	public String getGenero() {
		return genero;
	}

	public void setGenero(String genero) {
		this.genero = genero;
	}
	public String getTemporada() {
		return temporada;
	}

	public void setTemporada(String temporada) {
		this.temporada = temporada;
	}

	/**********************************************************/
	public void addLote(int a, int b, LocalDate c) {
			Lote p = new Lote(a, b, c);
				this.lotes.add(p);
	}
	
	public Lote getLote(int p){
			Lote P = null;
				if (p > 1 || p < this.lotes.size())
					P = this.lotes.get(p - 1);
						return P;
	}
	/**********************************************************/
	public float costoxLote(float costoxUnidad) {
			float p = 0;
				return p;
	}
	/**********************************************************/
	@Override
	public String toString() {
		return "Prenda [modelo=" + modelo + ", genero=" + genero + ", temporada=" + temporada + ", "
				+ "tela=" + tela + ", costoProduccion=" + costoProduccion + " lotes=" + lotes + "]";
	}
}
