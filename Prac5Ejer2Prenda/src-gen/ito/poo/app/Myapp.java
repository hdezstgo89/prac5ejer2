package ito.poo.app;

import java.time.LocalDate;
import ito.poo.clases.Lote;
import ito.poo.clases.Prenda;

@SuppressWarnings("unused")
public class Myapp {
			
	static void run() {
	Prenda prenda = new Prenda(132,"Masculino", "Verano","Lana", 140.50F);
	System.out.println(prenda);
	prenda.addLote(1, 5500, LocalDate.now());
	prenda.addLote(2, 2400, LocalDate.of(2022, 06, 01));
	System.out.println();
	System.out.println(prenda);
				
	System.out.println();
	System.out.println(prenda.getLote(2));
				
	System.out.println();
	System.out.println(prenda.getLote(1).costoProduccion(prenda));
	System.out.println(prenda.getLote(2).costoProduccion(prenda));
				
	System.out.println();
	System.out.println(prenda.getLote(1).montoProduccionxlote(prenda));
	System.out.println(prenda.getLote(1).montoRecuperacionxpieza(prenda));
	}

	public static void main(String[] args) {
		run();
	}
}
 